using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo( "CK.Setup.SqlServer.Tests, PublicKey = 002400000480000094000000060200000024000052534131000400000" +
                                                                      "10001002badda7c6774254194bd7d7b264aa7be4622e8a0105acfe1b" +
                                                                      "2edc239b3389a317e008862dd5c62b61298042874b8bf08c4ad18a71" +
                                                                      "dcbae5234066d3f6ef159bc9f8014c89d5be68f4d5b59af4169f1578" +
                                                                      "4af3eb2fa02e312e480ea123f383c09bab56a016b46519cc830fa17b" +
                                                                      "d6ccff7260cc8d20ece42745cef70b98e3c70d9" )]

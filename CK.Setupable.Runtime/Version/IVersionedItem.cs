#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.Setupable.Runtime\Version\IVersionedItem.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;

namespace CK.Setup
{
    /// <summary>
    /// Defines the identity of a versionable <see cref="IDependentItem"/>.
    /// </summary>
    public interface IVersionedItem : IDependentItem
	{        
        /// <summary>
        /// Gets an identifier of the type of the item. This is required
        /// in order to be able to handle specific storage for version without 
        /// relying on any <see cref="IDependentItem.FullName">FullName</see> conventions and may 
        /// help troubleshooting.
        /// Must be a non null, nor empty empty or whitespace identifier of at most 16 characters long.
        /// </summary>
        string ItemType { get; }

        /// <summary>
        /// Gets current version. 
        /// Null if no version exists or applies to this object.
        /// </summary>
        Version Version { get; }

        /// <summary>
        /// Gets an optionnal list of <see cref="VersionedName"/>. <see cref="VersionedName.FullName"/> in this list
        /// are not null and the list is sorted by <see cref="VersionedName.Version">Version</see> in ascending order.
        /// Can be null if no previous names exists.
        /// </summary>
        IEnumerable<VersionedName> PreviousNames { get; }
	}
}

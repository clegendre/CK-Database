#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.StObj.Engine\StObj\Impl\MutableItem.StructuredObject.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using CK.Core;
using System.Reflection;
using CK.CodeGen;

namespace CK.Setup
{

    partial class MutableItem
    {

        public object CreateStructuredObject( IActivityMonitor monitor, IStObjRuntimeBuilder runtimeBuilder )
        {
            Debug.Assert( Specialization == null );
            Debug.Assert( _leafData.StructuredObject == null, "Called once and only once." );
            try
            {
                return _leafData.CreateStructuredObject( runtimeBuilder, ObjectType );
            }
            catch( Exception ex )
            {
                monitor.Error( ex );
                return null;
            }
        }

        /// <summary>
        /// Gets the properties to set right before the call to StObjConstruct.
        /// Properties are registered at the root object, the Property.DeclaringType can be used to
        /// target the correct type in the inheritance chain.
        /// </summary>
        public IReadOnlyList<PropertySetter> PreConstructProperties => _preConstruct;

        //public string GetFinalTypeCSharpName( IActivityMonitor monitor, IDynamicAssembly a )
        //{
        //    Debug.Assert( Specialization == null );
        //    return _leafData.ImplementableTypeInfo == null
        //                ? ObjectType.ToCSharpName() 
        //                : _leafData.ImplementableTypeInfo.GenerateType( monitor, a );
        //}

        /// <summary>
        /// Gets the post build properties to set. Potentially not null only on leaves.
        /// </summary>
        public IReadOnlyList<PropertySetter> PostBuildProperties => _leafData?.PostBuildProperties;


    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CK.Core
{
    /// <summary>
    /// This interface marker states that a class or an interface instance
    /// is the base of an <see cref="IAmbientContract"/>. 
    /// </summary>
    public interface IAmbientContractDefiner
    {
    }


}

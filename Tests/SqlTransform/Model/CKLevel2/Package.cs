﻿using CK.Setup;
using CK.SqlServer;
using CK.SqlServer.Setup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKLevel2
{
    [SqlPackage( ResourcePath = "Res", Schema = "CK" )]
    [Versions("0.0.0")]
    public abstract class Package : SqlPackage
    {
        [SqlProcedure( "replace:sSimpleReplaceTest" )]
        public abstract string SimpleReplaceTest( ISqlCallContext ctx, string textParam, int added );

        [SqlProcedure( "transform:sSimpleTransformTest" )]
        public abstract string SimpleTransformTest( ISqlCallContext ctx, string textParam, int added );
    }
}
